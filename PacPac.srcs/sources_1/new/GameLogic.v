//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   GameLogic
 Author:        Grzegorz W�grzyn
 Version:       1.0
 Last modified: 2017-05-30
 Coding style: safe, with FPGA sync reset
 Description:  Main module of Logic
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module GameLogic(
        input wire clk,
        input wire rst,
        
        input wire [11:0] mouse_xpos,
        input wire [11:0] mouse_ypos,
        input wire mouse_left,
        input wire mouse_right,
        input wire [11:0] ext_mouse_xpos,
        input wire [11:0] ext_mouse_ypos,
        input wire ext_mouse_left,
       // input wire ext_mouse_right,
        
        output wire [8:0] xy_t1,
        output wire [8:0] xy_t2,
        output wire menu_activ,
        output wire score_activ,
        output wire draw_target,
        output wire [1:0] game_mode,
        
        output wire [6:0] score1,
        output wire [6:0] score2
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
    localparam  SEC     =   40_000_000,
                MIN     =   60*SEC,
                INFINITE =   5*MIN;
    localparam  TIME_TARGET_NORMAL  =   2*SEC,
                TIME_TARGET_MULTI   =   INFINITE,
                TIME_TARGET_REFLEX  =   SEC - 13,
                TIME_GAME_NORMAL    =   INFINITE,
                TIME_GAME_MULTI     =   30*SEC,
                TIME_GAME_REFLEX    =   MIN;              
//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    wire [8:0] rng2top_xy_t1, rng2top_xy_t2;
    
    wire [8:0] rng2logic_xy_t1, rng2logic_xy_t2;
    wire logic2rng_stop1, logic2rng_stop2;
    
    wire top2logic_mouse_left, top2logic_mouse_right, top2logic_ext_mouse_left;// top2logic_ext_mouse_right;
    wire [11:0] top2logic_mouse_xpos, top2logic_mouse_ypos, top2logic_ext_mouse_xpos, top2logic_ext_mouse_ypos;
    wire logic2top_activ_menu, logic2top_score_activ, logic2top_draw_target;
    
    wire logic2target_timeout_en, logic2game_timeout_en, logic2target_timeout_rst, logic2game_timeout_rst;
    wire [31:0] logic2target_timeout_max_val, logic2game_timeout_max_val;
    
    wire target_timeout2logic_timeout, game_timeout2logic_timeout;
    wire [1:0] logic2target_timeout_game_state, logic2game_timeout_game_state;
    wire [6:0] logic2top_score1, logic2top_score2;
    
    wire [1:0] logic2top_game_mode;
    
    reg [31:0] target_time [2:0];
    reg [31:0] game_time [2:0];
    
    initial
    begin
        target_time[0] = TIME_TARGET_NORMAL;
        target_time[1] = TIME_TARGET_MULTI;
        target_time[2] = TIME_TARGET_REFLEX;
        game_time[0] = TIME_GAME_NORMAL;
        game_time[1] = TIME_GAME_MULTI;
        game_time[2] = TIME_GAME_REFLEX;
    end
    
    Timeout #( .WIDTH(32))
    target_timeout
    (
        .clk(clk),
        .rst(rst || logic2target_timeout_rst),
        .en(logic2target_timeout_en),
        .max_val(target_time[logic2target_timeout_game_state]),
        .timeout(target_timeout2logic_timeout)
    );
    Timeout #( .WIDTH(32))
    game_timeout
    (
        .clk(clk),
        .rst(rst || logic2game_timeout_rst),
        .en(logic2game_timeout_en),
        .max_val(game_time[logic2game_timeout_game_state]),
        .timeout(game_timeout2logic_timeout)
    );
    RNG #( .WIDTH(9), .MAXVAL(299))
    RNG1
    (
        .clk(clk),
        .rst(rst),
        .stop(logic2rng_stop1),
        
        .random(rng2top_xy_t1)
    );
    RNG #( .WIDTH(9), .MAXVAL(302))
    RNG2
    (
        .clk(clk),
        .rst(rst),
        .stop(logic2rng_stop2),
        
        .random(rng2top_xy_t2)
    );
    Logic #( )
    my_Logic
    (
        .clk(clk),
        .rst(rst),
        
        .mouse_left(top2logic_mouse_left),
        .mouse_right(top2logic_mouse_right),
        .mouse_xpos(top2logic_mouse_xpos),
        .mouse_ypos(top2logic_mouse_ypos),
        .ext_mouse_left(top2logic_ext_mouse_left),
        //.ext_mouse_right(top2logic_ext_mouse_right),
        .ext_mouse_xpos(top2logic_ext_mouse_xpos),
        .ext_mouse_ypos(top2logic_ext_mouse_ypos),
        
        .xy_t1(rng2logic_xy_t1),
        .xy_t2(rng2logic_xy_t2),
        
        .timeout_target(target_timeout2logic_timeout),
        .timeout_game(game_timeout2logic_timeout),
        
        .game_mode(logic2target_timeout_game_state),
        .menu_activ(logic2top_menu_activ),
        .score_activ(logic2top_score_activ),
        .draw_target(logic2top_draw_target),
        .stop_RNG1(logic2rng_stop1),
        .stop_RNG2(logic2rng_stop2),
        
        .timeout_target_en(logic2target_timeout_en),
        .timeout_target_rst(logic2target_timeout_rst),
        .timeout_game_en(logic2game_timeout_en),
        .timeout_game_rst(logic2game_timeout_rst),
        
        .score1_out(logic2top_score1),
        .score2_out(logic2top_score2)
    );
    
    assign  xy_t1 = rng2top_xy_t1,
            xy_t2 = rng2top_xy_t2;
    assign  rng2logic_xy_t1 = rng2top_xy_t1,
            rng2logic_xy_t2 = rng2top_xy_t2;
    assign  top2logic_mouse_left = mouse_left,
            top2logic_mouse_right = mouse_right,
            top2logic_mouse_xpos = mouse_xpos,
            top2logic_mouse_ypos = mouse_ypos,
            top2logic_ext_mouse_left = ext_mouse_left,
         //   top2logic_ext_mouse_right = ext_mouse_right,
            top2logic_ext_mouse_xpos = ext_mouse_xpos,
            top2logic_ext_mouse_ypos = ext_mouse_ypos;
    assign  menu_activ = logic2top_menu_activ,
            score_activ = logic2top_score_activ,
            draw_target = logic2top_draw_target;
    assign  logic2game_timeout_game_state = logic2target_timeout_game_state;
    assign  logic2top_game_mode = logic2game_timeout_game_state,
            game_mode = logic2top_game_mode,
            score1  =   logic2top_score1,
            score2  =   logic2top_score2;
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

endmodule
