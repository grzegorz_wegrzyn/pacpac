//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   menu_rom
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  ROM with menu graphics
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module menu_rom
	(
	    input wire en,
		input wire clk, // posedge active clock
		input wire [17 : 0] address,
		output reg [2 : 0] coded_rgb = 0
	);

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
	(* rom_style = "block" *) // block || distributed
    //(* cascade_height = 4 *)
    //(* dont_touch = "yes" *)
    reg [2:0] rom [262143:0]; // rom memory
    
    initial
        $readmemh("menu_512x512_rotated_coded.data", rom, 0, 262143); 

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
	always @(posedge clk) begin
	   if(en)
        coded_rgb <= #1 rom[address];
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

endmodule
