//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   register
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  40MHz sampling via register
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module register(
    input wire clk,
    input wire rst,
    input wire [11:0] xpos_in,
    input wire [11:0] ypos_in,
    input wire left_in,
    input wire right_in,
    output reg [11:0] xpos_out = 0,
    output reg [11:0] ypos_out = 0,
    output reg left_out = 0,
    output reg right_out = 0
    );
//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

always @(posedge clk)
    if(rst)
        begin
            xpos_out <= #1 0;
            ypos_out <= #1 0;
            left_out <= #1 0;
            right_out <= #1 0;
        end
    else
        begin
            xpos_out <= #1 xpos_in;
            ypos_out <= #1 ypos_in;
            left_out <= #1 left_in;
            right_out <= #1 right_in;
        end

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

endmodule
