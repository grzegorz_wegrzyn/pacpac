//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   background
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Display background
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module background(
        input wire rst,
        input wire clk40MHz,
        input wire [10:0] hcount_in,
        input wire hsync_in,
        input wire hblnk_in,
        input wire [10:0] vcount_in,
        input wire vsync_in,
        input wire vblnk_in,
        output reg [10:0] hcount_out    = 0,
        output reg hsync_out            = 0,
        output reg hblnk_out            = 0,
        output reg [10:0] vcount_out    = 0,   
        output reg vsync_out            = 0,
        output reg vblnk_out            = 0,
        output reg [11:0] rgb_out       = 0
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [11:0] rgb_out_nxt = 0;

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk40MHz)
    begin
        if(rst)
        begin 
            hsync_out <= #1 0;
            vsync_out <= #1 0;
            hblnk_out <= #1 0;
            vblnk_out <= #1 0;
            hcount_out <= #1 0;
            vcount_out <= #1 0;
            rgb_out <= #1 0;  
        end
        else
        begin
            hsync_out <= #1 hsync_in;
            vsync_out <= #1 vsync_in;
            hblnk_out <= #1 hblnk_in;
            vblnk_out <= #1 vblnk_in;
            hcount_out <= #1 hcount_in;
            vcount_out <= #1 vcount_in;
            rgb_out <= #1 rgb_out_nxt;  
        end  
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
    begin         
        // During blanking, make it it black.
        if (vblnk_in || hblnk_in) rgb_out_nxt = 12'h0_0_0; 
        //Draw background
        else
            begin
                // Active display, top edge, make a yellow line.
                if (vcount_in == 0)
                    rgb_out_nxt = 12'hf_f_0;
                // Active display, bottom edge, make a red line.
                else if (vcount_in == 599)
                    rgb_out_nxt = 12'hf_0_0;
                // Active display, left edge, make a green line.
                else if (hcount_in == 0)
                    rgb_out_nxt = 12'h0_f_0;
                // Active display, right edge, make a blue line.
                else if (hcount_in == 799)
                    rgb_out_nxt = 12'h0_0_f;
                else
                    rgb_out_nxt = 12'h0_0_0;
            end  
    end     

endmodule
