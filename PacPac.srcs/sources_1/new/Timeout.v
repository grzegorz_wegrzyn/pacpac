//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   Timeout
 Author:        Grzegorz W�grzyn
 Version:       1.0
 Last modified: 2017-06-02
 Coding style: safe, with FPGA sync reset
 Description:  Timeout timer
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module Timeout
    #( parameter
        WIDTH = 32
    )
    (
        input wire clk,
        input wire rst,
        input wire en,
        input wire [WIDTH - 1:0] max_val,
        
        output reg timeout = 0
    );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [WIDTH - 1:0] counter, counter_nxt = 0;
    reg timeout_nxt = 0;
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk)
    begin
        if(rst)
        begin
            timeout <= #1 0;
            counter <= #1 0;
        end
        else
        begin
            timeout <= #1 timeout_nxt;
            counter <= #1 counter_nxt;
        end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

    always @*
    begin
        if(en)
            if(counter == max_val)
            begin
                counter_nxt = 0;
                timeout_nxt = 1;
            end
            else
            begin
                counter_nxt = counter + 1;
                timeout_nxt = 0;
            end
        else
        begin
            counter_nxt = counter;
            timeout_nxt = timeout;
        end
    end

endmodule
