//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   score_rom
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  ROM with score graphics
 */
//////////////////////////////////////////////////////////////////////////////

module score_rom(
    input wire en,
    input wire clk, // posedge active clock
    input wire [14:0] address,
    output reg coded_rgb = 0
    );
    
//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    (* rom_style = "block" *) // block || distributed
    //(* cascade_height = 4 *)
    //(* dont_touch = "yes" *)
    reg rom [32767:0]; // rom memory
    
    initial
        $readmemh("score_512x64_rotated_coded.data", rom, 0, 32767); 

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk) begin
       if(en)
        coded_rgb <= #1 rom[address];
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

endmodule

