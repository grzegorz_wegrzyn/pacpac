`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2017 23:26:40
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test();

    reg rst, clk;
    wire h_sync, v_sync;
    wire [3:0] r, g, b;


        
    PacPac my_PacPac(
        .clk(clk),
        .rst(rst),
        .h_sync(h_sync),
        .v_sync(v_sync),
        .r(r),
        .g(g),
        .b(b)
    );


always
begin
    #5
    clk = 1;
    #5
    clk = 0;
end

initial
begin
    rst = 0;
end

endmodule
