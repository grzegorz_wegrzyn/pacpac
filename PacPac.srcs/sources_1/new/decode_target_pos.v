//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   decode_target_pos
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-25
 Coding style: safe, with FPGA sync reset
 Description:  Decode xpos and ypos of the target
 */
//////////////////////////////////////////////////////////////////////////////

module decode_target_pos(
    input wire clk,
    input wire rst,
    input wire [8:0] target_pos,
    output reg [9:0] target_decoded_xpos,
    output reg [9:0] target_decoded_ypos 
    );
    
//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------   
    

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [9:0] target_decoded_xpos_nxt, target_decoded_ypos_nxt;

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk)
        if(rst)
            begin
                target_decoded_xpos <= #1 0;
                target_decoded_ypos <= #1 0;
            end
        else
            begin
                target_decoded_xpos <= #1 target_decoded_xpos_nxt;
                target_decoded_ypos <= #1 target_decoded_ypos_nxt;
            end

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
always @*
case(target_pos)
0:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 0;
    end
1:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 0;
    end            
2:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 0;
    end
3:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 0;
    end
4:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 0;
    end
5:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 0;
    end
6:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 0;
    end
7:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 0;
    end
8:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 0;
    end
9:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 0;
    end
10: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 0;
    end
11: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 0;
    end
12: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 0;
    end
13: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 0;
    end         
14: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 0;
    end
15: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 0;
    end            
16: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 0;
    end
17: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 0;
    end
18: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 0;
    end
19: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 0;
    end
20:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 40;
        end
21:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 40;
    end            
22:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 40;
    end
23:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 40;
    end
24:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 40;
    end
25:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 40;
    end
26:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 40;
    end
27:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 40;
    end
28:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 40;
    end
29:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 40;
    end
30: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 40;
    end
31: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 40;
    end
32: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 40;
    end
33: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 40;
    end         
34: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 40;
    end
35: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 40;
    end            
36: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 40;
    end
37: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 40;
    end
38: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 40;
    end
39: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 40;
    end
40:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 80;
    end
41:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 80;
    end            
42:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 80;
    end
43:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 80;
    end
44:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 80;
    end
45:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 80;
    end
46:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 80;
    end
47:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 80;
    end
48:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 80;
    end
49:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 80;
    end
50: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 80;
    end
51: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 80;
    end
52: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 80;
    end
53: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 80;
    end         
54: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 80;
    end
55: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 80;
    end            
56: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 80;
    end
57: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 80;
    end
58: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 80;
    end
59: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 80;
    end
60:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 120;
    end
61:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 120;
    end            
62:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 120;
    end
63:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 120;
    end
64:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 120;
    end
65:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 120;
    end
66:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 120;
    end
67:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 120;
    end
68:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 120;
    end
69:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 120;
    end
70: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 120;
    end
71: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 120;
    end
72: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 120;
    end
73: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 120;
    end         
74: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 120;
    end
75: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 120;
    end            
76: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 120;
    end
77: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 120;
    end
78: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 120;
    end
79: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 0;
    end
80:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 160;
    end
81:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 160;
    end            
82:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 160;
    end
83:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 160;
    end
84:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 160;
    end
85:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 160;
    end
86:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 160;
    end
87:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 160;
    end
88:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 160;
    end
89:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 160;
    end
90: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 160;
    end
91: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 160;
    end
92: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 160;
    end
93: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 160;
    end         
94: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 160;
    end
95: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 160;
    end            
96: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 160;
    end
97: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 160;
    end
98: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 160;
    end
99: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 160;
    end
100:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 200;
    end
101:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 200;
    end            
102:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 200;
    end
103:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 200;
    end
104:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 200;
    end
105:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 200;
    end
106:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 200;
    end
107:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 200;
    end
108:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 200;
    end
109:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 200;
    end
110: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 200;
    end
111: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 200;
    end
112: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 200;
    end
113: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 200;
    end         
114: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 200;
    end
115: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 200;
    end            
116: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 200;
    end
117: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 200;
    end
118: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 200;
    end
119: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 200;
    end 
120:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 240;
    end
121:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 240;
    end            
122:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 240;
    end
123:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 240;
    end
124:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 240;
    end
125:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 240;
    end
126:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 240;
    end
127:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 240;
    end
128:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 240;
    end
129:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 240;
    end
130: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 240;
    end
131: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 240;
    end
132: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 240;
    end
133: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 240;
    end         
134: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 240;
    end
135: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 240;
    end            
136: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 240;
    end
137: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 240;
    end
138: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 240;
    end
139: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 240;
    end 
140:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 280;
    end
141:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 280;
    end            
142:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 280;
    end
143:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 280;
    end
144:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 280;
    end
145:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 280;
    end
146:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 280;
    end
147:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 280;
    end
148:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 280;
    end
149:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 280;
    end
150: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 280;
    end
151: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 280;
    end
152: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 280;
    end
153: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 280;
    end         
154: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 280;
    end
155: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 280;
    end            
156: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 280;
    end
157: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 280;
    end
158: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 280;
    end
159: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 280;
    end 
160:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 320;
    end
161:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 320;
    end            
162:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 320;
    end
163:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 320;
    end
164:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 320;
    end
165:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 320;
    end
166:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 320;
    end
167:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 320;
    end
168:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 320;
    end
169:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 320;
    end
170: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 320;
    end
171: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 320;
    end
172: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 320;
    end
173: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 320;
    end         
174: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 320;
    end
175: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 320;
    end            
176: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 320;
    end
177: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 320;
    end
178: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 320;
    end
179: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 320;
    end
180:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 360;
    end
181:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 360;
    end            
182:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 360;
    end
183:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 360;
    end
184:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 360;
    end
185:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 360;
    end
186:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 360;
    end
187:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 360;
    end
188:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 360;
    end
189:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 360;
    end
190: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 360;
    end
191: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 360;
    end
192: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 360;
    end
193: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 360;
    end         
194: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 360;
    end
195: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 360;
    end            
196: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 360;
    end
197: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 360;
    end
198: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 360;
    end
199: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 360;
    end  
200:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 400;
    end
201:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 400;
    end            
202:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 400;
    end
203:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 400;
    end
204:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 400;
    end
205:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 400;
    end
206:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 400;
    end
207:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 400;
    end
208:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 400;
    end
209:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 400;
    end
210: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 400;
    end
211: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 400;
    end
212: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 400;
    end
213: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 400;
    end         
214: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 400;
    end
215: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 400;
    end            
216: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 400;
    end
217: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 400;
    end
218: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 400;
    end
219: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 400;
    end  
220:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 440;
    end
221:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 440;
    end            
222:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 440;
    end
223:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 440;
    end
224:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 440;
    end
225:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 440;
    end
226:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 440;
    end
227:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 440;
    end
228:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 440;
    end
229:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 440;
    end
230: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 440;
    end
231: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 440;
    end
232: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 440;
    end
233: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 440;
    end         
234: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 440;
    end
235: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 440;
    end            
236: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 440;
    end
237: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 440;
    end
238: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 440;
    end
239: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 440;
    end 
240:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 480;
    end
241:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 480;
    end            
242:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 480;
    end
243:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 480;
    end
244:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 480;
    end
245:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 480;
    end
246:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 480;
    end
247:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 480;
    end
248:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 480;
    end
249:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 480;
    end
250: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 480;
    end
251: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 480;
    end
252: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 480;
    end
253: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 480;
    end         
254: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 480;
    end
255: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 480;
    end            
256: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 480;
    end
257: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 480;
    end
258: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 480;
    end
259: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 480;
    end
260:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 520;
    end
261:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 520;
    end            
262:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 520;
    end
263:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 520;
    end
264:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 520;
    end
265:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 520;
    end
266:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 520;
    end
267:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 520;
    end
268:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 520;
    end
269:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 520;
    end
270: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 520;
    end
271: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 520;
    end
272: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 520;
    end
273: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 520;
    end         
274: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 520;
    end
275: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 520;
    end            
276: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 520;
    end
277: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 520;
    end
278: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 520;
    end
279: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 520;
    end
280:  begin
        target_decoded_xpos_nxt = 0;
        target_decoded_ypos_nxt = 560;
    end
281:  begin
        target_decoded_xpos_nxt = 40;
        target_decoded_ypos_nxt = 560;
    end            
282:  begin
        target_decoded_xpos_nxt = 80;
        target_decoded_ypos_nxt = 560;
    end
283:  begin
        target_decoded_xpos_nxt = 120;
        target_decoded_ypos_nxt = 560;
    end
284:  begin
        target_decoded_xpos_nxt = 160;
        target_decoded_ypos_nxt = 560;
    end
285:  begin
        target_decoded_xpos_nxt = 200;
        target_decoded_ypos_nxt = 560;
    end
286:  begin
        target_decoded_xpos_nxt = 240;
        target_decoded_ypos_nxt = 560;
    end
287:  begin
        target_decoded_xpos_nxt = 280;
        target_decoded_ypos_nxt = 560;
    end
288:  begin
        target_decoded_xpos_nxt = 320;
        target_decoded_ypos_nxt = 560;
    end
289:  begin
        target_decoded_xpos_nxt = 360;
        target_decoded_ypos_nxt = 560;
    end
290: begin
        target_decoded_xpos_nxt = 400;
        target_decoded_ypos_nxt = 560;
    end
291: begin
        target_decoded_xpos_nxt = 440;
        target_decoded_ypos_nxt = 560;
    end
292: begin
        target_decoded_xpos_nxt = 480;
        target_decoded_ypos_nxt = 560;
    end
293: begin
        target_decoded_xpos_nxt = 520;
        target_decoded_ypos_nxt = 560;
    end         
294: begin
        target_decoded_xpos_nxt = 560;
        target_decoded_ypos_nxt = 560;
    end
295: begin
        target_decoded_xpos_nxt = 600;
        target_decoded_ypos_nxt = 560;
    end            
296: begin
        target_decoded_xpos_nxt = 640;
        target_decoded_ypos_nxt = 560;
    end
297: begin
        target_decoded_xpos_nxt = 680;
        target_decoded_ypos_nxt = 560;
    end
298: begin
        target_decoded_xpos_nxt = 720;
        target_decoded_ypos_nxt = 560;
    end
299: begin
        target_decoded_xpos_nxt = 760;
        target_decoded_ypos_nxt = 560;
    end                                
default: begin
            target_decoded_xpos_nxt = 0;
            target_decoded_ypos_nxt = 0;
        end
endcase
    
endmodule
