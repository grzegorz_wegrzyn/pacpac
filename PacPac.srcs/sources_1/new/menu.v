
//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   menu
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Display menu graphics
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module menu(
        input wire rst,
        input wire clk40MHz,
        input wire menu_activ,
        input wire [10:0] hcount_in,
        input wire hsync_in,
        input wire hblnk_in,
        input wire [10:0] vcount_in,
        input wire vsync_in,
        input wire vblnk_in,
        input wire [11:0] rgb_in,
        input wire [2:0] coded_rgb_pixel,
        input wire [11:0] mouse_xpos,
        input wire [11:0] mouse_ypos,
        output reg [10:0] hcount_out    = 0,
        output reg hsync_out            = 0,
        output reg hblnk_out            = 0,
        output reg [10:0] vcount_out    = 0,   
        output reg vsync_out            = 0,
        output reg vblnk_out            = 0,
        output reg [11:0] rgb_out       = 0,
        output reg [17:0] pixel_addr    = 0
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
    localparam PIXEL_DELAY  = 2;
    
    localparam MENU_XPOS    = 144;
    localparam MENU_YPOS    =  44;
    localparam MENU_WIDTH   = 512;
    localparam MENU_HEIGHT  = 512;
    
    localparam NORMAL_XPOS  = 260;
    localparam NORMAL_YPOS  = 224;
    localparam HARD_XPOS    = 260;
    localparam HARD_YPOS    = 334;
    localparam REFLEX_XPOS  = 260;
    localparam REFLEX_YPOS  = 444;
    localparam BTN_WIDTH    = 280;
    localparam BTN_HEIGHT   =  80;
//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
     reg [10:0] hcount_in_1, hcount_in_2, vcount_in_1, vcount_in_2 = 0;
     reg hsync_in_1, hsync_in_2, vsync_in_1, vsync_in_2 = 0;
     reg hblnk_in_1, hblnk_in_2, vblnk_in_1, vblnk_in_2 = 0; 
     reg [11:0] rgb_in_1, rgb_in_2 = 0;
     reg [11:0] rgb_out_nxt = 0;
     reg [8:0] addr_x = 0;
     reg [8:0] addr_y = 0;

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk40MHz)              //delay 1 clk
    begin
        if(rst)
            begin
                hcount_out  <= #1 0;
                hsync_out   <= #1 0;
                hblnk_out   <= #1 0;
                vcount_out  <= #1 0;
                vsync_out   <= #1 0;
                vblnk_out   <= #1 0;
                rgb_out     <= #1 0;
                pixel_addr  <= #1 0;
                
                hcount_in_2  <= #1 0;
                hsync_in_2   <= #1 0;
                hblnk_in_2   <= #1 0;
                vcount_in_2  <= #1 0;
                vsync_in_2   <= #1 0;
                vblnk_in_2   <= #1 0;   
                rgb_in_2     <= #1 0;
                                    
                hcount_in_1  <= #1 0;
                hsync_in_1   <= #1 0;
                hblnk_in_1   <= #1 0;
                vcount_in_1  <= #1 0;
                vsync_in_1   <= #1 0;
                vblnk_in_1   <= #1 0;   
                rgb_in_1     <= #1 0;     
            end
        else if (menu_activ == 0)
            begin
                hcount_out  <= #1 hcount_in;
                hsync_out   <= #1 hsync_in;
                hblnk_out   <= #1 hblnk_in;
                vcount_out  <= #1 vcount_in;
                vsync_out   <= #1 vsync_in;
                vblnk_out   <= #1 vblnk_in;
                rgb_out     <= #1 rgb_in;
                pixel_addr  <= #1 0;
            end
        else
            begin
                hsync_in_1 <= #1 hsync_in;
                vsync_in_1 <= #1 vsync_in;
                hblnk_in_1 <= #1 hblnk_in;
                vblnk_in_1 <= #1 vblnk_in;
                hcount_in_1 <= #1 hcount_in;
                vcount_in_1 <= #1 vcount_in;            
                rgb_in_1 <= #1 rgb_in;       
        
                hsync_in_2 <= #1 hsync_in_1;       //delay 2 clk
                vsync_in_2 <= #1 vsync_in_1;
                hblnk_in_2 <= #1 hblnk_in_1;
                vblnk_in_2 <= #1 vblnk_in_1;
                hcount_in_2 <= #1 hcount_in_1;
                vcount_in_2 <= #1 vcount_in_1;
                rgb_in_2 <= #1 rgb_in_1;    
        
                hsync_out <= #1 hsync_in_2;        //output
                vsync_out <= #1 vsync_in_2;
                hblnk_out <= #1 hblnk_in_2;
                vblnk_out <= #1 vblnk_in_2;
                hcount_out <= #1 hcount_in_2;
                vcount_out <= #1 vcount_in_2;            
                rgb_out <= #1 rgb_out_nxt;
                pixel_addr <= #1 {addr_y, addr_x};
            end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
    begin
        if((hcount_in >= MENU_XPOS && hcount_in <= MENU_XPOS + MENU_WIDTH - 1 ) && (vcount_in >= MENU_YPOS && vcount_in <= MENU_YPOS + MENU_HEIGHT - 1) && ~(hblnk_in || vblnk_in))
        begin     
            addr_x = hcount_in - MENU_XPOS;                   
            addr_y = vcount_in - MENU_YPOS;     
        end
        else
        begin
            {addr_y, addr_x} = pixel_addr;
        end
        
        if((hcount_in >= MENU_XPOS + PIXEL_DELAY && hcount_in <= MENU_XPOS + MENU_WIDTH - 1 + PIXEL_DELAY ) && (vcount_in >= MENU_YPOS && vcount_in <= MENU_YPOS + MENU_HEIGHT - 1) && ~(hblnk_in || vblnk_in))
        begin        
            case(coded_rgb_pixel)               //decoding colors from rom
                0: rgb_out_nxt = 12'h0_0_0;
                1: rgb_out_nxt = 12'hf_f_0;
                2: rgb_out_nxt = (mouse_xpos>NORMAL_XPOS && mouse_xpos<NORMAL_XPOS+BTN_WIDTH && mouse_ypos>NORMAL_YPOS && mouse_ypos<NORMAL_YPOS+BTN_HEIGHT ) ? 12'hf_f_f : 12'hf_a_c;
                3: rgb_out_nxt = (mouse_xpos>HARD_XPOS && mouse_xpos<HARD_XPOS+BTN_WIDTH && mouse_ypos>HARD_YPOS && mouse_ypos<HARD_YPOS+BTN_HEIGHT )         ? 12'hf_f_f : 12'he_1_2;
                4: rgb_out_nxt = (mouse_xpos>REFLEX_XPOS && mouse_xpos<REFLEX_XPOS+BTN_WIDTH && mouse_ypos>REFLEX_YPOS && mouse_ypos<REFLEX_YPOS+BTN_HEIGHT ) ? 12'hf_f_f : 12'hb_e_1;
                5: rgb_out_nxt = (mouse_xpos>HARD_XPOS && mouse_xpos<HARD_XPOS+BTN_WIDTH && mouse_ypos>HARD_YPOS && mouse_ypos<HARD_YPOS+BTN_HEIGHT )         ? 12'hf_f_f : 12'he_1_1;
                default: rgb_out_nxt = 12'h0_0_0;
            endcase
        end
        else
        begin
            rgb_out_nxt = rgb_in_2;
        end
    end     

endmodule
