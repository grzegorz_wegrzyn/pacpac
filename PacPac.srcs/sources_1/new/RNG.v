//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   RNG
 Author:        Grzegorz W�grzyn
 Version:       1.0
 Last modified: 2017-05-30
 Coding style: safe, with FPGA sync reset
 Description:  Random Number Generatr
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module RNG
    #( parameter
        WIDTH = 9,
        MAXVAL = 299
    )
    (
        input wire stop,
        input wire clk,
        input wire rst,
        output reg [WIDTH - 1 : 0] random = 0
    );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [WIDTH - 1:0] counter, counter_nxt = 0;
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk)
    begin
        if(rst)
        begin
            counter <= #1 0;
        end
        else
        begin
            counter <= #1 counter_nxt;
        end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

    always @*
    begin
        if(counter == MAXVAL)
            counter_nxt = 0;
        else
            counter_nxt = counter + 1;
    end
    
    always @(posedge stop)
        random <= #1 counter_nxt;

endmodule
