//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   PacPac
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Top module
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module PacPac(
    input wire clk,
    input wire rst,
    
    input wire [10:0] ext_mouse_xpos,
    input wire [10:0] ext_mouse_ypos,
    input wire ext_mouse_left,
   // input wire ext_mouse_right,
    
    output reg v_sync   = 0,
    output reg h_sync   = 0,
    output reg [3:0] r  = 0,
    output reg [3:0] g  = 0,
    output reg [3:0] b  = 0,
    
    inout wire ps2_clk,
    inout wire ps2_data
    );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    clk my_clk(
        .clk(clk),
        .reset(rst),
        .clk100MHz(clk100MHz),
        .clk40MHz(clk40MHz)
    );
    
    
    
    wire [11:0] mouse2VGA_xpos, mouse2gamelogic_xpos, mouse2VGA_ypos,  mouse2gamelogic_ypos;
    wire mouse2VGA_left, mouse2VGA_right, mouse2gamelogic_left, mouse2gamelogic_right;
    
    wire top2gamelogic_ext_left;// top2gamelogic_ext_right;
    wire [10:0] top2gamelogic_ext_xpos, top2gamelogic_ext_ypos;
    
    assign  mouse2gamelogic_xpos = mouse2VGA_xpos,
            mouse2gamelogic_ypos = mouse2VGA_ypos,
            mouse2gamelogic_left = mouse2VGA_left,
            mouse2gamelogic_right = mouse2VGA_right;
            
    assign  top2gamelogic_ext_xpos = ext_mouse_xpos,
            top2gamelogic_ext_ypos = ext_mouse_ypos,
            top2gamelogic_ext_left = ext_mouse_left;
     //       top2gamelogic_ext_right = ext_mouse_right;
            
    mouse_controller my_mouse_controller(
        .clk100MHz(clk100MHz),
        .clk40MHz(clk40MHz),
        .rst(rst),
        .mouse_xpos(mouse2VGA_xpos),
        .mouse_ypos(mouse2VGA_ypos),
        .mouse_left(mouse2VGA_left),
        .mouse_right(mouse2VGA_right),
        .ps2_clk(ps2_clk),
        .ps2_data(ps2_data)
    );
    
    wire [8:0] gamelogic2VGA_xy_t1, gamelogic2VGA_xy_t2;
    wire gamelogic2VGA_menu_activ, gamelogic2VGA_score_activ, gamelogic2VGA_draw_target;
    wire [1:0] gamelogic2VGA_game_mode;
    wire [6:0] gamelogic2VGA_score1, gamelogic2VGA_score2;
    GameLogic my_game_logic(
        .clk(clk40MHz),
        .rst(rst),
        
        .mouse_xpos(mouse2gamelogic_xpos),
        .mouse_ypos(mouse2gamelogic_ypos),
        .mouse_left(mouse2gamelogic_left),
        .mouse_right(mouse2gamelogic_right),
        .ext_mouse_xpos({1'b0, top2gamelogic_ext_xpos}),
        .ext_mouse_ypos({1'b0, top2gamelogic_ext_ypos}),
        .ext_mouse_left(top2gamelogic_ext_left),
       // .ext_mouse_right(top2gamelogic_ext_right),
        
        .xy_t1(gamelogic2VGA_xy_t1),
        .xy_t2(gamelogic2VGA_xy_t2),
        .menu_activ(gamelogic2VGA_menu_activ),
        .score_activ(gamelogic2VGA_score_activ),
        .draw_target(gamelogic2VGA_draw_target),
        .game_mode(gamelogic2VGA_game_mode),
        
        .score1(gamelogic2VGA_score1),
        .score2(gamelogic2VGA_score2)
    );
        
    wire [3:0] vga2top_r, vga2top_g, vga2top_b; 
    VGA_controller my_VGA_controller(
        .rst(rst),
        .clk40MHz(clk40MHz),
        .menu_activ(gamelogic2VGA_menu_activ),
        .score_activ(gamelogic2VGA_score_activ),
        .draw_target(gamelogic2VGA_draw_target),
        .game_mod(gamelogic2VGA_game_mode),
        .score1(gamelogic2VGA_score1),
        .score2(gamelogic2VGA_score2),
        .mouse_xpos(mouse2VGA_xpos),
        .mouse_ypos(mouse2VGA_ypos),
        .ext_mouse_xpos({1'b0, ext_mouse_xpos}),
        .ext_mouse_ypos({1'b0, ext_mouse_ypos}),
        .xy_t1(gamelogic2VGA_xy_t1),
        .xy_t2(gamelogic2VGA_xy_t2),
        .hs(vga2top_hs),
        .vs(vga2top_vs),
        .r(vga2top_r),
        .g(vga2top_g),
        .b(vga2top_b)
    );
    
    
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
        begin
            h_sync = vga2top_hs;
            v_sync = vga2top_vs;
            
            r = vga2top_r;
            g = vga2top_g;
            b = vga2top_b;
        end

endmodule
