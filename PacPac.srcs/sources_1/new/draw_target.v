
//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   draw_target
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Display targets
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module draw_target(
        input wire rst,
        input wire clk40MHz,        
        input wire draw_target,
        input wire [9:0] target1_xpos,
        input wire [9:0] target1_ypos,
        input wire [9:0] target2_xpos,
        input wire [9:0] target2_ypos,
        input wire [10:0] hcount_in,
        input wire hsync_in,
        input wire hblnk_in,
        input wire [10:0] vcount_in,
        input wire vsync_in,
        input wire vblnk_in,
        input wire [11:0] rgb_in,
        output reg [10:0] hcount_out    = 0,
        output reg hsync_out            = 0,
        output reg hblnk_out            = 0,
        output reg [10:0] vcount_out    = 0,   
        output reg vsync_out            = 0,
        output reg vblnk_out            = 0,
        output reg [11:0] rgb_out       = 0
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
        localparam TARGET1_COLOR  = 12'h0_A_A;
        localparam TARGET1_WIDTH  = 40;
        localparam TARGET1_HEIGHT = 40;

        localparam TARGET2_COLOR  = 12'hA_5_0;
        localparam TARGET2_WIDTH  = 40;
        localparam TARGET2_HEIGHT = 40;
//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
        reg [11:0] rgb_nxt =0;
        
        reg [10:0] hcount1 =0, vcount1 =0;
        reg hsync1, hblnk1, vsync1, vblnk1;
        reg [11:0] rgb1;

//------------------------------------------------------------------------------
// internal 1 clk delay
//------------------------------------------------------------------------------
    always @(posedge clk40MHz)
    begin
        hcount1 <= #1 hcount_in;
        hsync1 <= #1 hsync_in;
        hblnk1 <= #1 hblnk_in;
        vcount1 <= #1 vcount_in;
        vsync1 <= #1 vsync_in;
        vblnk1 <= #1 vblnk_in;
        rgb1 <= #1 rgb_nxt;
    end

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk40MHz)
    begin
        if(rst)
            begin
                hcount_out <= #1 0;
                hsync_out <= #1 0;
                hblnk_out <= #1 0;
                vcount_out <= #1 0;
                vsync_out <= #1 0;
                vblnk_out <= #1 0;
                rgb_out <= #1 0;
            end
        else if (draw_target == 0)
            begin
                hcount_out <= #1 hcount_in;
                hsync_out <= #1 hsync_in;
                hblnk_out <= #1 hblnk_in;
                vcount_out <= #1 vcount_in;
                vsync_out <= #1 vsync_in;
                vblnk_out <= #1 vblnk_in;
                rgb_out <= #1 rgb_in;
            end
        else
            begin
                hcount_out <= #1 hcount1;
                hsync_out <= #1 hsync1;
                hblnk_out <= #1 hblnk1;
                vcount_out <= #1 vcount1;
                vsync_out <= #1 vsync1;
                vblnk_out <= #1 vblnk1;
                rgb_out <= #1 rgb1;
            end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
        begin
            if ( (hcount_in >= target1_xpos) && (hcount_in < target1_xpos + TARGET1_WIDTH) && (vcount_in >=target1_ypos) && (vcount_in < target1_ypos + TARGET1_HEIGHT) && (hblnk_in==0) && (vblnk_in==0) )
                rgb_nxt = TARGET1_COLOR;
            else if ( (hcount_in >= target2_xpos) && (hcount_in < target2_xpos + TARGET2_WIDTH) && (vcount_in >=target2_ypos) && (vcount_in < target2_ypos + TARGET2_HEIGHT) && (hblnk_in==0) && (vblnk_in==0) )
                rgb_nxt = TARGET2_COLOR;            
            else
                rgb_nxt = rgb_in;
        end

endmodule