//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   VGA_controller
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Main module of VGA
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module VGA_controller(
        input wire rst,
        input wire clk40MHz,
        input wire menu_activ,
        input wire score_activ,
        input wire [1:0] game_mod,
        input wire [6:0] score1,
        input wire [6:0] score2,
        input wire draw_target,
        input wire [11:0] mouse_xpos,
        input wire [11:0] mouse_ypos, 
        
        input wire [11:0] ext_mouse_xpos,
        input wire [11:0] ext_mouse_ypos,
         
        input wire [8:0] xy_t1,
        input wire [8:0] xy_t2,
        output reg hs       = 0,
        output reg vs       = 0,
        output reg [3:0] r  = 0,
        output reg [3:0] g  = 0,
        output reg [3:0] b  = 0
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    wire [10:0] timing2background_hcount, timing2background_vcount;    
    wire timing2background_vsync, timing2background_hsync, timing2background_vblnk, timing2background_hblnk;
    vga_timing my_vga_timing(
        .rst(rst),
        .clk(clk40MHz),
        .vcount(timing2background_vcount),        //[10:0]
        .vsync(timing2background_vsync),
        .vblnk(timing2background_vblnk),
        .hcount(timing2background_hcount),        //[10:0]
        .hsync(timing2background_hsync),
        .hblnk(timing2background_hblnk)
    );
    
    wire [10:0] background2menu_hcount, background2menu_vcount;
    wire [11:0] background2menu_rgb;
    wire background2menu_vsync, background2menu_hsync, background2menu_vblnk, background2menu_hblnk;
    background my_background(
        .rst(rst),
        .clk40MHz(clk40MHz),
        .vcount_in(timing2background_vcount),        //[10:0]
        .vsync_in(timing2background_vsync),
        .vblnk_in(timing2background_vblnk),
        .hcount_in(timing2background_hcount),        //[10:0]
        .hsync_in(timing2background_hsync),
        .hblnk_in(timing2background_hblnk),
        .hcount_out(background2menu_hcount),         //[10:0]
        .hsync_out(background2menu_hsync),
        .hblnk_out(background2menu_hblnk),
        .vcount_out(background2menu_vcount),         //[10:0]
        .vsync_out(background2menu_vsync),
        .vblnk_out(background2menu_vblnk),
        .rgb_out(background2menu_rgb)                //[11:0]
    );
    
    wire [10:0] menu2DrawTarget_hcount, menu2DrawTarget_vcount;
    wire [11:0] menu2DrawTarget_rgb;
    wire [2 :0] menuROM2menu_RGBpixel;
    wire [17:0] menu2menuROM_PixelAddr;
    wire menu2DrawTarget_vsync, menu2DrawTarget_hsync, menu2DrawTarget_vblnk, menu2DrawTarget_hblnk;
    menu my_menu(
        .clk40MHz(clk40MHz),
        .rst(rst),
        .menu_activ(menu_activ),
        .vcount_in(background2menu_vcount),     //[10:0]
        .vsync_in(background2menu_vsync),
        .vblnk_in(background2menu_vblnk),
        .hcount_in(background2menu_hcount),     //[10:0]
        .hsync_in(background2menu_hsync),
        .hblnk_in(background2menu_hblnk),
        .rgb_in(background2menu_rgb),
        .coded_rgb_pixel(menuROM2menu_RGBpixel),      //[2:0]
        .mouse_xpos(mouse_xpos),
        .mouse_ypos(mouse_ypos),
        .hcount_out(menu2DrawTarget_hcount),    //[10:0]
        .hsync_out(menu2DrawTarget_hsync),
        .hblnk_out(menu2DrawTarget_hblnk),
        .vcount_out(menu2DrawTarget_vcount),    //[10:0]
        .vsync_out(menu2DrawTarget_vsync),
        .vblnk_out(menu2DrawTarget_vblnk),
        .rgb_out(menu2DrawTarget_rgb),          //[11:0]
        .pixel_addr(menu2menuROM_PixelAddr)      //[11:0]
    );
    
    menu_rom my_menu_rom(
        .en(1'b1),
        .clk(clk40MHz),
        .address(menu2menuROM_PixelAddr),        //[11:0]
        .coded_rgb(menuROM2menu_RGBpixel)        //[2:0]
    );
    
    wire [9 :0] DecodePos2DrawTarget_xpos_t1, DecodePos2DrawTarget_ypos_t1;
    wire [9 :0] DecodePos2DrawTarget_xpos_t2, DecodePos2DrawTarget_ypos_t2;
    wire [10:0] DrawTarget2score_hcount, DrawTarget2score_vcount;
    wire [11:0] DrawTarget2score_rgb;
    wire DrawTarget2score_vsync, DrawTarget2score_hsync, DrawTarget2score_vblnk, DrawTarget2score_hblnk;
    draw_target my_draw_target(
        .rst(rst),
        .clk40MHz(clk40MHz),    
        .draw_target(draw_target),
        .target1_xpos(DecodePos2DrawTarget_xpos_t1),        //[9:0]
        .target1_ypos(DecodePos2DrawTarget_ypos_t1),        //[9:0]
        .target2_xpos(DecodePos2DrawTarget_xpos_t2),        //[9:0]
        .target2_ypos(DecodePos2DrawTarget_ypos_t2),        //[9:0]
        .hcount_in(menu2DrawTarget_hcount),    //[10:0]
        .hsync_in(menu2DrawTarget_hsync),
        .hblnk_in(menu2DrawTarget_hblnk),
        .vcount_in(menu2DrawTarget_vcount),    //[10:0]
        .vsync_in(menu2DrawTarget_vsync),
        .vblnk_in(menu2DrawTarget_vblnk),
        .rgb_in(menu2DrawTarget_rgb),          //[11:0]
        .hcount_out(DrawTarget2score_hcount),  //[10:0]
        .hsync_out(DrawTarget2score_hsync),
        .hblnk_out(DrawTarget2score_hblnk),
        .vcount_out(DrawTarget2score_vcount),  //[10:0]
        .vsync_out(DrawTarget2score_vsync),
        .vblnk_out(DrawTarget2score_vblnk),
        .rgb_out(DrawTarget2score_rgb)         //[11:0]
    );
    
    decode_target_pos my_decode_target1_pos(
        .clk(clk40MHz),
        .rst(rst),
        .target_pos(xy_t1),
        .target_decoded_xpos(DecodePos2DrawTarget_xpos_t1),
        .target_decoded_ypos(DecodePos2DrawTarget_ypos_t1)
    );
    
    decode_target_pos my_decode_target2_pos(
        .clk(clk40MHz),
        .rst(rst),
        .target_pos(xy_t2),
        .target_decoded_xpos(DecodePos2DrawTarget_xpos_t2),
        .target_decoded_ypos(DecodePos2DrawTarget_ypos_t2)
    );
    
    wire [10:0] score2MouseDisplay_hcount, score2MouseDisplay_vcount;
    wire [11:0] score2MouseDisplay_rgb;
    wire [14:0] score2scoreROM_PixelAddr;  
    wire scoreROM2score_RGBpixel;
    wire score2hsyncdelay_hsync, score2vsyncdelay_vsync, score2MouseDisplay_vblnk, score2MouseDisplay_hblnk;   
    wire [7:0] fontROM2score_CharPixels;
    wire [7:0] score2charROM_charXY;
    wire [6:0] charROM2fontROM_CharCode;
    wire [3:0] score2fontROM_CharLine;
    wire [10:0] addr;
    score my_score(
        .rst(rst),
        .clk40MHz(clk40MHz),
        .score_activ(score_activ),
        .hcount_in(DrawTarget2score_hcount),    //[10:0]
        .hsync_in(DrawTarget2score_hsync),
        .hblnk_in(DrawTarget2score_hblnk),
        .vcount_in(DrawTarget2score_vcount),    //[10:0]
        .vsync_in(DrawTarget2score_vsync),
        .vblnk_in(DrawTarget2score_vblnk),
        .rgb_in(DrawTarget2score_rgb),          //[11:0]
        .coded_rgb_pixel(scoreROM2score_RGBpixel),
        .char_pixels(fontROM2score_CharPixels),
        .hcount_out(score2MouseDisplay_hcount),          //[10:0]
        .hsync_out(score2hsyncdelay_hsync),
        .hblnk_out(score2MouseDisplay_hblnk),
        .vcount_out(score2MouseDisplay_vcount),          //[10:0]
        .vsync_out(score2vsyncdelay_vsync),
        .vblnk_out(score2MouseDisplay_vblnk),
        .rgb_out(score2MouseDisplay_rgb),               //[11:0]
        .pixel_addr(score2scoreROM_PixelAddr),          //[14:0]
        .char_xy(score2charROM_charXY),                              //[7:0]
        .char_line(score2fontROM_CharLine)                           //[3:0]
    );
    
    score_rom my_score_rom(
        .en(1'b1),
        .clk(clk40MHz),
        .address(score2scoreROM_PixelAddr),
        .coded_rgb(scoreROM2score_RGBpixel)
    );
    
    char_rom_16x16 my_char_rom_16x16(
        .char_xy(score2charROM_charXY),
        .game_mod(game_mod),
        .score1(score1),
        .score2(score2),
        .char_code(charROM2fontROM_CharCode)
    );
    
    assign addr = {charROM2fontROM_CharCode, score2fontROM_CharLine};
    
    font_rom my_font_rom(
        .clk(clk40MHz),
        .addr(addr),
        .char_line_pixels(fontROM2score_CharPixels)
    );
    
    wire hsyncdelay2top_hsync, vsyncdelay2top_vsync;
    wire hsyncdelay_22top_hsync, vsyncdelay_22top_vsync;
    delay #(1, 1)
    my_hsync_delay(
        .clk(clk40MHz),
        .rst(rst),
        .din(score2hsyncdelay_hsync),
        .dout(hsyncdelay2top_hsync)
    );
    
    delay #(1, 1)
    my_vsync_delay(
        .clk(clk40MHz),
        .rst(rst),
        .din(score2vsyncdelay_vsync),
        .dout(vsyncdelay2top_vsync)
    );
    
    delay #(1, 2)
    my_hsync_delay_2(
        .clk(clk40MHz),
        .rst(rst),
        .din(score2hsyncdelay_hsync),
        .dout(hsyncdelay_22top_hsync)
    );
    
    delay #(1, 2)
    my_vsync_delay_2(
        .clk(clk40MHz),
        .rst(rst),
        .din(score2vsyncdelay_vsync),
        .dout(vsyncdelay_22top_vsync)
    );
    
    wire [11:0] MouseDisplay2extMouseDisplay_rgb;
    wire blank;
    assign blank = score2MouseDisplay_hblnk || score2MouseDisplay_vblnk;
    MouseDisplay my_mouse_display(
        .pixel_clk(clk40MHz),
        .xpos(mouse_xpos),                                  //[11:0]
        .ypos(mouse_ypos),                                  //[11:0]
        .hcount(score2MouseDisplay_hcount),                 //[10:0]
        .vcount(score2MouseDisplay_vcount),                 //[10:0]
        .blank(blank),
        .red_in(score2MouseDisplay_rgb[11:8]),              //[3:0]
        .green_in(score2MouseDisplay_rgb[7:4]),             //[3:0]
        .blue_in(score2MouseDisplay_rgb[3:0]),              //[3:0]
        .enable_mouse_display_out(),
        .red_out(MouseDisplay2extMouseDisplay_rgb[11:8]),               //[3:0]                                   
        .green_out(MouseDisplay2extMouseDisplay_rgb[7:4]),              //[3:0]
        .blue_out(MouseDisplay2extMouseDisplay_rgb[3:0])                //[3:0]
    );
    
    reg top2extMouseDisplay_blank, top2extMouseDisplay_blank_nxt = 0;
    reg [10:0] score2MouseDisplay_hcount_delayed, score2MouseDisplay_vcount_delayed = 0;
    wire [11:0] MouseDisplay2top_rgb;
    MouseDisplay my_ext_mouse_display(
        .pixel_clk(clk40MHz),
        .xpos(ext_mouse_xpos),                                  //[11:0]
        .ypos(ext_mouse_ypos),                                  //[11:0]
        .hcount(score2MouseDisplay_hcount_delayed),                 //[10:0]
        .vcount(score2MouseDisplay_vcount_delayed),                 //[10:0]
        .blank(top2extMouseDisplay_blank),
        .red_in(MouseDisplay2extMouseDisplay_rgb[11:8]),              //[3:0]
        .green_in(MouseDisplay2extMouseDisplay_rgb[7:4]),             //[3:0]
        .blue_in(MouseDisplay2extMouseDisplay_rgb[3:0]),              //[3:0]
        .enable_mouse_display_out(),
        .red_out(MouseDisplay2top_rgb[11:8]),               //[3:0]                                   
        .green_out(MouseDisplay2top_rgb[7:4]),              //[3:0]
        .blue_out(MouseDisplay2top_rgb[3:0])                //[3:0]
    );
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

    //----- GW:
    always @(posedge clk40MHz)
    begin
        if(rst)
        begin
            top2extMouseDisplay_blank <= #1 0;
            score2MouseDisplay_hcount_delayed <= #1 0;
            score2MouseDisplay_vcount_delayed <= #1 0;
        end
        else
        begin
            top2extMouseDisplay_blank <= #1 top2extMouseDisplay_blank_nxt;
            score2MouseDisplay_hcount_delayed <= #1 score2MouseDisplay_hcount;
            score2MouseDisplay_vcount_delayed <= #1 score2MouseDisplay_vcount;
        end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
    begin
        if(game_mod == 1)
        begin
            hs = hsyncdelay_22top_hsync;
            vs = vsyncdelay_22top_vsync;
        end
        else
        begin
            hs = hsyncdelay2top_hsync;
            vs = vsyncdelay2top_vsync;        
        end
        
        if(game_mod == 0 || game_mod == 2)
        begin
            r = MouseDisplay2extMouseDisplay_rgb[11:8];
            g = MouseDisplay2extMouseDisplay_rgb[7:4];
            b = MouseDisplay2extMouseDisplay_rgb[3:0];
        end
        else
        begin
            r = MouseDisplay2top_rgb[11:8];
            g = MouseDisplay2top_rgb[7:4];
            b = MouseDisplay2top_rgb[3:0];
        end
        
        //---------- GW:
        if(blank)
            top2extMouseDisplay_blank_nxt = 1;
        else
            top2extMouseDisplay_blank_nxt = 0;    
    end

endmodule
