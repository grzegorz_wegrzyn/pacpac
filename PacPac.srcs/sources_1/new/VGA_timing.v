//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   VGA_timing
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Generate VGA waveforms
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module vga_timing(
        input wire rst,
        input  wire clk,
        output wire [10:0] vcount,
        output wire vsync,
        output wire vblnk,
        output wire [10:0] hcount,
        output wire hsync,
        output wire hblnk
    );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
    localparam TRUE = 1;
    localparam FALSE = 0;
    
    localparam HOR_PIXELS = 800;
    localparam HOR_TOTAL_TIME = 1056;
    localparam HOR_BLANK_START = 800;
    localparam HOR_BLANK_TIME = 256;
    localparam HOR_SYNC_START = 840;
    localparam HOR_SYNC_TIME = 128;
    localparam HOR_SYNC_END = 968;   //HOR_SYNC_START + HOR_SYNC_TIME;
    
    localparam VER_PIXELS = 600;
    localparam VER_TOTAL_TIME = 628;
    localparam VER_BLANK_START = 600;
    localparam VER_BLANK_TIME = 28;
    localparam VER_SYNC_START = 601;
    localparam VER_SYNC_TIME = 4;
    localparam VER_SYNC_END = 605;   //VER_SYNC_START + VER_SYNC_TIME;

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg h_sync,
        h_blnk,
        v_sync,
        v_blnk                  = 0;
    reg [10:0] h_count,
               h_count_nxt,
               v_count,
               v_count_nxt      = 0;
    
    assign hcount = h_count;
    assign hblnk  = h_blnk;
    assign hsync  = h_sync;
    assign vcount = v_count;
    assign vblnk  = v_blnk;
    assign vsync  = v_sync;

//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk)
    begin
        if(rst)
        begin
            h_count <= #1 0;
            v_count <= #1 0;
            h_blnk  <= #1 0;
            h_sync  <= #1 0;
            v_blnk  <= #1 0;
            v_sync  <= #1 0;
        end
        else
        begin
            h_count <= #1 h_count_nxt;
            v_count <= #1 v_count_nxt;
            
            if (h_count_nxt < HOR_PIXELS)
                h_blnk <= #1 FALSE;
            else
                h_blnk <= #1 TRUE;
            if ( (h_count_nxt >= HOR_SYNC_START) & (h_count_nxt < HOR_SYNC_END) )
                h_sync <= #1 TRUE;
            else
                h_sync <= #1 FALSE;
            if (v_count_nxt < VER_PIXELS)
                v_blnk <= #1 FALSE;
            else
                v_blnk <= #1 TRUE;
            if ( (v_count_nxt >= VER_SYNC_START) & (v_count_nxt < VER_SYNC_END) )
                v_sync <= #1 TRUE;
            else
                v_sync <= #1 FALSE;
        end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
    begin
       if (h_count < (HOR_TOTAL_TIME - 1))
           begin
               h_count_nxt = h_count + 1;
               v_count_nxt = v_count;
           end        
       else 
           begin
               h_count_nxt = 0;
               if (v_count < VER_TOTAL_TIME )
                   begin
                       v_count_nxt = v_count + 1;
                   end
               else
                   v_count_nxt = 0;    
           end
    end

endmodule
