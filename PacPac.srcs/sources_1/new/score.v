//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   score
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Display score
 */
//////////////////////////////////////////////////////////////////////////////

module score(
    input wire rst,
    input wire clk40MHz,
    input wire score_activ,
    input wire [10:0] hcount_in,
    input wire hsync_in,
    input wire hblnk_in,
    input wire [10:0] vcount_in,
    input wire vsync_in,
    input wire vblnk_in,
    input wire [11:0] rgb_in,
    input wire coded_rgb_pixel,
    input wire [7:0] char_pixels,
    output reg [10:0] hcount_out    = 0,
    output reg hsync_out            = 0,
    output reg hblnk_out            = 0,
    output reg [10:0] vcount_out    = 0,   
    output reg vsync_out            = 0,
    output reg vblnk_out            = 0,
    output reg [11:0] rgb_out       = 0,
    output reg [14:0] pixel_addr    = 0,
    output reg [7:0] char_xy        = 0,
    output reg [3:0] char_line      = 0
    );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
    localparam PIXEL_DELAY  = 2;
    
    localparam GAMEOVER_XPOS    = 144;
    localparam GAMEOVER_YPOS    = 175;
    localparam GAMEOVER_WIDTH   = 512;
    localparam GAMEOVER_HEIGHT  =  64;

    localparam SCORE_XPOS       = 336;
    localparam SCORE_YPOS       = 256;
    localparam SCORE_WIDTH      = 128;
    localparam SCORE_HEIGHT     = 256;
    
                            
//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [10:0] hcount_in_1, hcount_in_2, vcount_in_1, vcount_in_2 = 0;
    reg hsync_in_1, hsync_in_2, vsync_in_1, vsync_in_2 = 0;
    reg hblnk_in_1, hblnk_in_2, vblnk_in_1, vblnk_in_2 = 0; 
    reg [11:0] rgb_in_1, rgb_in_2 = 0;
    reg [11:0] rgb_out_nxt = 0;
    reg [8:0] addr_x = 0;
    reg [5:0] addr_y = 0;
    reg [7:0] char_xy_nxt;
    reg [3:0] char_line_nxt;
    reg [11:0] score_xpos_to_draw=0, score_ypos_to_draw=0;
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk40MHz)
    begin
        if(rst)
            begin
                hcount_out  <= #1 0;
                hsync_out   <= #1 0;
                hblnk_out   <= #1 0;
                vcount_out  <= #1 0;
                vsync_out   <= #1 0;
                vblnk_out   <= #1 0;
                rgb_out     <= #1 0;
                pixel_addr  <= #1 0;
                
                hcount_in_2  <= #1 0;
                hsync_in_2   <= #1 0;
                hblnk_in_2   <= #1 0;
                vcount_in_2  <= #1 0;
                vsync_in_2   <= #1 0;
                vblnk_in_2   <= #1 0;   
                rgb_in_2     <= #1 0;
                                    
                hcount_in_1  <= #1 0;
                hsync_in_1   <= #1 0;
                hblnk_in_1   <= #1 0;
                vcount_in_1  <= #1 0;
                vsync_in_1   <= #1 0;
                vblnk_in_1   <= #1 0;   
                rgb_in_1     <= #1 0;
                
                                        char_xy     <= 0;
                                        char_line   <= 0;
            end
        else if (score_activ == 0)
            begin
                hcount_out  <= #1 hcount_in;
                hsync_out   <= #1 hsync_in;
                hblnk_out   <= #1 hblnk_in;
                vcount_out  <= #1 vcount_in;
                vsync_out   <= #1 vsync_in;
                vblnk_out   <= #1 vblnk_in;
                rgb_out     <= #1 rgb_in;
                pixel_addr  <= #1 0;
                
                                        char_xy     <= 0;
                                        char_line   <= 0;
            end
        else
            begin
                hsync_in_1 <= #1 hsync_in;
                vsync_in_1 <= #1 vsync_in;
                hblnk_in_1 <= #1 hblnk_in;
                vblnk_in_1 <= #1 vblnk_in;
                hcount_in_1 <= #1 hcount_in;
                vcount_in_1 <= #1 vcount_in;            
                rgb_in_1 <= #1 rgb_in;       
        
                hsync_in_2 <= #1 hsync_in_1;       //delay 2 clk
                vsync_in_2 <= #1 vsync_in_1;
                hblnk_in_2 <= #1 hblnk_in_1;
                vblnk_in_2 <= #1 vblnk_in_1;
                hcount_in_2 <= #1 hcount_in_1;
                vcount_in_2 <= #1 vcount_in_1;
                rgb_in_2 <= #1 rgb_in_1;    
        
                hsync_out <= #1 hsync_in_2;        //output
                vsync_out <= #1 vsync_in_2;
                hblnk_out <= #1 hblnk_in_2;
                vblnk_out <= #1 vblnk_in_2;
                hcount_out <= #1 hcount_in_2;
                vcount_out <= #1 vcount_in_2;            
                rgb_out <= #1 rgb_out_nxt;
                pixel_addr <= #1 {addr_y, addr_x};
                
                                        char_xy <= char_xy_nxt;
                                        char_line <= char_line_nxt;
            end
    end
    
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
always @*           //draw "gameover"
    begin
    
        score_xpos_to_draw = (SCORE_XPOS%8 == 0) ? SCORE_XPOS : SCORE_XPOS-SCORE_XPOS%8;      
        score_ypos_to_draw = (SCORE_YPOS%16 == 0) ? SCORE_YPOS : SCORE_YPOS-SCORE_YPOS%16;
    
        char_xy_nxt <= {hcount_in[6:3], vcount_in[7:4]} - score_xpos_to_draw*2;
        char_line_nxt <= vcount_in[3:0]; 
    
        if((hcount_in >= GAMEOVER_XPOS && hcount_in <= GAMEOVER_XPOS + GAMEOVER_WIDTH - 1 ) && (vcount_in >= GAMEOVER_YPOS && vcount_in <= GAMEOVER_YPOS + GAMEOVER_HEIGHT - 1) && ~(hblnk_in || vblnk_in))
            begin     
                addr_x = hcount_in - GAMEOVER_XPOS;                   
                addr_y = vcount_in - GAMEOVER_YPOS;     
            end
        else
            begin
                {addr_y, addr_x} = pixel_addr;
            end
        
        if((hcount_in >= GAMEOVER_XPOS + PIXEL_DELAY && hcount_in <= GAMEOVER_XPOS + GAMEOVER_WIDTH - 1 + PIXEL_DELAY ) && (vcount_in >= GAMEOVER_YPOS && vcount_in <= GAMEOVER_YPOS + GAMEOVER_HEIGHT - 1) && ~(hblnk_in || vblnk_in))
            begin        
                case(coded_rgb_pixel)               //decoding colors from rom
                    0: rgb_out_nxt = 12'h0_0_0;
                    1: rgb_out_nxt = 12'hf_f_0;
                    default: rgb_out_nxt = 12'h0_0_0;
                endcase
            end
            
        else if (vcount_in_2 >= score_ypos_to_draw && vcount_in_2 < score_ypos_to_draw+SCORE_HEIGHT)
            if (hcount_in_2 >= score_xpos_to_draw && hcount_in_2 < score_xpos_to_draw+SCORE_WIDTH)                 
                if (hcount_in_2 % 8 == 7 && char_pixels[0]) rgb_out_nxt = 12'hfff;
                else if (hcount_in_2 % 8 == 6 && char_pixels[1]) rgb_out_nxt = 12'hfff;
                else if (hcount_in_2 % 8 == 5 && char_pixels[2]) rgb_out_nxt = 12'hfff; 
                else if (hcount_in_2 % 8 == 4 && char_pixels[3]) rgb_out_nxt = 12'hfff; 
                else if (hcount_in_2 % 8 == 3 && char_pixels[4]) rgb_out_nxt = 12'hfff; 
                else if (hcount_in_2 % 8 == 2 && char_pixels[5]) rgb_out_nxt = 12'hfff;
                else if (hcount_in_2 % 8 == 1 && char_pixels[6]) rgb_out_nxt = 12'hfff; 
                else if (hcount_in_2 % 8 == 0 && char_pixels[7]) rgb_out_nxt = 12'hfff;
                else rgb_out_nxt = 12'h000;   
            else
                rgb_out_nxt = rgb_in_2;
            
        else
            begin
                rgb_out_nxt = rgb_in_2;
            end          
    end     

endmodule
