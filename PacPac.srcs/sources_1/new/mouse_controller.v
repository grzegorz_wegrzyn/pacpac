//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   mouse_controler
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style: safe, with FPGA sync reset
 Description:  Main module of mouse control
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module mouse_controller(
    input wire clk100MHz,
    input wire clk40MHz,
    input wire rst,
    output reg [11:0] mouse_xpos = 0,
    output reg [11:0] mouse_ypos = 0,
    output reg mouse_left = 0,
    output reg mouse_right = 0,
    inout wire ps2_clk,
    inout wire ps2_data
);
//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------

wire [11:0] MouseMvCtl2reg40MHz_xpos, MouseMvCtl2reg40MHz_ypos;
wire MouseMvCtl2reg40MHz_left, MouseMvCtl2reg40MHz_right;
wire MouseMvCtl2reg40MHz_ps2data;
MouseCtl mouse_move_control(
    .clk(clk100MHz),
    .rst(rst),
    .xpos(MouseMvCtl2reg40MHz_xpos),        //[11:0]
    .ypos(MouseMvCtl2reg40MHz_ypos),        //[11:0]
    .zpos(),
    .left(MouseMvCtl2reg40MHz_left),
    .middle(),
    .right(MouseMvCtl2reg40MHz_right),
    .new_event(),
    .value(0),
    .setx(0),
    .sety(0),
    .setmax_x(0),
    .setmax_y(0),
    .ps2_clk(ps2_clk),
    .ps2_data(ps2_data)
);

wire [11:0] reg40MHz2top_xpos, reg40MHz2top_ypos;
//wire reg40MHz2top_left, reg40MHz2top_right;
wire reg40MHz2debounce_left, reg40MHz2top_right;
wire reg40MHz2top_ps2data;
register reg40MHz(
    .clk(clk40MHz),
    .rst(rst),
    .xpos_in(MouseMvCtl2reg40MHz_xpos),         //[11:0]
    .ypos_in(MouseMvCtl2reg40MHz_ypos),         //[11:0]
    .left_in(MouseMvCtl2reg40MHz_left),
    .right_in(MouseMvCtl2reg40MHz_right),
    .xpos_out(reg40MHz2top_xpos),               //[11:0]
    .ypos_out(reg40MHz2top_ypos),               //[11:0]
    .left_out(reg40MHz2debounce_left),
    .right_out(reg40MHz2top_right)
);

wire debounce2top_left;
debounce my_debounce(
    .clk(clk40MHz),
    .reset(rst),
    .sw(reg40MHz2debounce_left),
    .db_tick(debounce2top_left),
    .db_level()
);
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
always @*
    begin
        mouse_xpos = reg40MHz2top_xpos;
        mouse_ypos = reg40MHz2top_ypos;
        //mouse_left = reg40MHz2top_left;
        mouse_left = debounce2top_left;
        mouse_right = reg40MHz2top_right;
    end
    
endmodule
