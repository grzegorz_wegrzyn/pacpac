//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   Logic
 Author:        Grzegorz Wegrzyn
 Version:       1.0
 Last modified: 2017-05-30
 Coding style: safe, with FPGA sync reset
 Description:  Logic unit, generating control signals and computing
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

    module Logic
        #(
            parameter
            TARGET_WIDTH = 40,
            TARGET_HEIGHT = 40,
            TIMEOUT = 100_000_000
        )
        (
        input wire clk,
        input wire rst,
        
        input wire [11:0] mouse_xpos,
        input wire [11:0] mouse_ypos,
        input wire mouse_left,
        input wire mouse_right,
        
        input wire [11:0] ext_mouse_xpos,
        input wire [11:0] ext_mouse_ypos,
        input wire ext_mouse_left,
       // input wire ext_mouse_right,
        
        input wire [8:0] xy_t1,
        input wire [8:0] xy_t2,
        
        input wire timeout_target,
        input wire timeout_game,
        
        output reg [1:0] game_mode  = 0,
        output reg menu_activ       = 0,
        output reg score_activ      = 0,
        output reg draw_target      = 0,
        output reg stop_RNG1        = 0,
        output reg stop_RNG2        = 0,
        
        output reg timeout_target_en    = 0,
        output reg timeout_game_en      = 0,
        output reg timeout_target_rst   = 0,
        output reg timeout_game_rst     = 0,
        
        output wire [6:0] score1_out,
        output wire [6:0] score2_out
        );

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------
    localparam  MENU = 4'b0000,
                RAND = 4'b0001,
               // SHOW = 4'b0010,
                WAIT = 4'b0011,
                DELETE = 4'b0100,
                SCORE = 4'b0101;
                
    localparam  NORMAL  = 2'b00,
                MULTI   = 2'b01,
                REFLEX  = 2'b10;
                
//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [1:0] game_mode_nxt = 0;
    reg [3:0] game_state_nxt, game_state = 0;
    reg menu_activ_nxt, score_activ_nxt, draw_target_nxt, stop_RNG1_nxt, stop_RNG2_nxt = 0;
    reg game_over, game_over_nxt = 0;     
         
    reg [6:0] score1, score1_nxt, score2, score2_nxt = 0;
             
    assign  score1_out = score1,
            score2_out = score2;         
             
    reg timeout_target_en_nxt, timeout_target_rst_nxt, timeout_game_en_nxt, timeout_game_rst_nxt = 0;         
             
    wire [9:0] xpos_t1, xpos_t2, ypos_t1, ypos_t2;
    
    decode_target_pos_combinatorial my_decode_t1(
        .xy_t(xy_t1),
        .target_decoded_xpos(xpos_t1),
        .target_decoded_ypos(ypos_t1)
    );
    decode_target_pos_combinatorial my_decode_t2(
        .xy_t(xy_t2),
        .target_decoded_xpos(xpos_t2),
        .target_decoded_ypos(ypos_t2)
    );
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------
    always @(posedge clk)
    begin
        if(rst)
        begin
            game_state  <= #1 MENU;
            game_mode   <= #1 NORMAL;
            menu_activ  <= #1 1;
            score_activ <= #1 0;
            draw_target <= #1 0;
            stop_RNG1   <= #1 0;
            stop_RNG2   <= #1 0;
            score1      <= #1 0;
            score2      <= #1 0;
            game_over   <= #1 0;
            timeout_target_en <= #1 0;
            timeout_game_en <= #1 0;
            timeout_target_rst <= #1 0;
            timeout_game_rst <= #1 0;
        end
        else
        begin
            game_state  <= #1 game_state_nxt;
            game_mode   <= #1 game_mode_nxt;
            menu_activ  <= #1 menu_activ_nxt;
            score_activ <= #1 score_activ_nxt;
            draw_target <= #1 draw_target_nxt;
            stop_RNG1   <= #1 stop_RNG1_nxt;
            stop_RNG2   <= #1 stop_RNG2_nxt;
            score1      <= #1 score1_nxt;
            score2      <= #1 score2_nxt;
            game_over   <= #1 game_over_nxt;
            timeout_target_en <= #1 timeout_target_en_nxt;
            timeout_game_en <= #1 timeout_game_en_nxt;
            timeout_target_rst <= #1 timeout_target_rst_nxt;
            timeout_game_rst <= #1 timeout_game_rst_nxt;
        end
    end
//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------
    always @*
    begin
        case (game_state)
            MENU:
            begin
                if(mouse_left && (mouse_xpos >= 260 && mouse_xpos <= 540) && (mouse_ypos >= 224 && mouse_ypos <= 304))
                begin
                    game_state_nxt = RAND;
                    game_mode_nxt = NORMAL;
                    menu_activ_nxt = 0;
                end
                else if(mouse_left && (mouse_xpos >= 260 && mouse_xpos <= 540) && (mouse_ypos >= 334 && mouse_ypos <= 414))
                begin
                    game_state_nxt = RAND;
                    game_mode_nxt = MULTI;
                    menu_activ_nxt = 0;
                end
                else if(mouse_left && (mouse_xpos >= 260 && mouse_xpos <= 540) && (mouse_ypos >= 444 && mouse_ypos <= 524))
                begin
                    game_state_nxt = RAND;
                    game_mode_nxt = REFLEX;
                    menu_activ_nxt = 0;
                end
                else
                begin
                    game_state_nxt = MENU;
                    game_mode_nxt = NORMAL;
                    menu_activ_nxt = 1;
                end
                score_activ_nxt = 0;
                draw_target_nxt = 0;
                stop_RNG1_nxt = 0;
                stop_RNG2_nxt = 0;
                score1_nxt = 0;
                score2_nxt = 0;
                game_over_nxt = 0;
                timeout_target_en_nxt = 0;
                timeout_target_rst_nxt = 0;
                timeout_game_en_nxt = 0;
                timeout_game_rst_nxt = 0;
            end
            
            RAND:
            begin
                game_state_nxt = WAIT;
                game_mode_nxt = game_mode;
                menu_activ_nxt = 0;
                score_activ_nxt = 0;
                draw_target_nxt = 0;
                stop_RNG1_nxt = 1;
                stop_RNG2_nxt = 1;
                score1_nxt = score1;
                score2_nxt = score2;
                game_over_nxt = 0;
                timeout_target_en_nxt = 1;
                timeout_target_rst_nxt = 0;
                timeout_game_en_nxt = 1;
                timeout_game_rst_nxt = 0;
            end
            
            WAIT:
            begin                
                if(timeout_target)
                begin
                    case (game_mode)
                        NORMAL:
                        begin
                            game_state_nxt = DELETE;  
                            game_over_nxt = 1;  
                        end
                        MULTI:
                        begin
                            game_state_nxt = WAIT;  
                            game_over_nxt = 0;  
                        end
                        REFLEX:
                        begin
                            game_state_nxt = DELETE;  
                            game_over_nxt = 0;     
                        end
                        default:
                        begin
                            game_state_nxt = MENU;  
                            game_over_nxt = 0; 
                        end
                    endcase
                    
                    score1_nxt = score1;
                    score2_nxt = score2;  
                end
                else if(timeout_game)
                begin
                    case (game_mode)
                        NORMAL:
                        begin
                            game_state_nxt = WAIT;
                            game_over_nxt = 0; 
                        end
                        MULTI:
                        begin
                            game_state_nxt = DELETE;
                            game_over_nxt = 1; 
                        end
                        REFLEX:
                        begin
                            game_state_nxt = DELETE;
                            game_over_nxt = 1; 
                        end
                        default:
                        begin
                            game_state_nxt = MENU;
                            game_over_nxt = 0; 
                        end
                    endcase
                    
                    score1_nxt = score1;
                    score2_nxt = score2;  
                end
                else if(mouse_left || ext_mouse_left)
                begin
                    if(mouse_left && (mouse_xpos >= xpos_t1 && mouse_xpos <= xpos_t1+TARGET_WIDTH) && (mouse_ypos >= ypos_t1 && mouse_ypos <= ypos_t1+TARGET_HEIGHT))
                        Player1_Hit;
                    else if(mouse_left && (mouse_xpos >= xpos_t2 && mouse_xpos <= xpos_t2+TARGET_WIDTH) && (mouse_ypos >= ypos_t2 && mouse_ypos <= ypos_t2+TARGET_HEIGHT))
                        Player1_Wrong;
                    else if(mouse_left)
                        Player1_Missed;
                    else if(ext_mouse_left && (ext_mouse_xpos >= xpos_t2 && ext_mouse_xpos <= xpos_t2+TARGET_WIDTH) && (ext_mouse_ypos >= ypos_t2 && ext_mouse_ypos <= ypos_t2+TARGET_HEIGHT))
                        Player2_Hit;
                    else if(ext_mouse_left && (ext_mouse_xpos >= xpos_t1 && ext_mouse_xpos <= xpos_t1+TARGET_WIDTH) && (ext_mouse_ypos >= ypos_t1 && ext_mouse_ypos <= ypos_t1+TARGET_HEIGHT))
                        Player2_Wrong;
                    else //if(ext_mouse_left)
                        Player2_Missed;    
                end
                else
                begin
                    game_state_nxt = WAIT;
                    score1_nxt = score1;
                    score2_nxt = score2;
                    game_over_nxt = 0;
                end
                
                game_mode_nxt = game_mode;
                menu_activ_nxt = 0;
                score_activ_nxt = 0;
                draw_target_nxt = 1;
                stop_RNG1_nxt = 0;
                stop_RNG2_nxt = 0;
                timeout_target_en_nxt = 1;
                timeout_target_rst_nxt = 0;
                timeout_game_en_nxt = 1;
                timeout_game_rst_nxt = 0;
            end
            
            DELETE:
            begin
                if(game_over)
                    game_state_nxt = SCORE;
                else
                    game_state_nxt = RAND;
                game_mode_nxt = game_mode;
                menu_activ_nxt = 0;
                score_activ_nxt = 0;
                draw_target_nxt = 0;
                stop_RNG1_nxt = 0;
                stop_RNG2_nxt = 0;
                score1_nxt = score1;
                score2_nxt = score2;
                game_over_nxt = 0;
                timeout_target_en_nxt = 0;
                timeout_target_rst_nxt = 1;
                timeout_game_en_nxt = 1;
                timeout_game_rst_nxt = 0;
            end
            
            SCORE:
            begin
                if(mouse_right)
                    game_state_nxt = MENU;
                else
                    game_state_nxt = SCORE;
                game_mode_nxt = game_mode;
                menu_activ_nxt = 0;
                score_activ_nxt = 1;
                draw_target_nxt = 0;
                stop_RNG1_nxt = 0;
                stop_RNG2_nxt = 0;
                score1_nxt = score1;
                score2_nxt = score2;
                game_over_nxt = 0;
                timeout_target_en_nxt = 0;
                timeout_target_rst_nxt = 1;
                timeout_game_en_nxt = 0;
                timeout_game_rst_nxt = 1;
            end
            
            default:
            begin
                game_state_nxt = MENU;
                game_mode_nxt = game_mode;
                menu_activ_nxt = 1;
                score_activ_nxt = 0;
                draw_target_nxt = 0;
                stop_RNG1_nxt = 0;
                stop_RNG2_nxt = 0;
                score1_nxt = 0;
                score2_nxt = 0;
                game_over_nxt = 0;
                timeout_target_en_nxt = 0;
                timeout_target_rst_nxt = 0;
                timeout_game_en_nxt = 0;
                timeout_game_rst_nxt = 0;
            end
        endcase
    end

task Player1_Hit;
begin
    game_state_nxt = DELETE;
    score1_nxt = score1 + 1;
    score2_nxt = score2;  
    game_over_nxt = 0;  
end
endtask

task Player2_Hit;
begin
    game_state_nxt = DELETE;
    score1_nxt = score1;
    score2_nxt = score2 + 1;  
    game_over_nxt = 0;  
end
endtask

task Player1_Missed;
begin
    if(game_mode == NORMAL)
    begin
        game_over_nxt = 1;
        game_state_nxt = DELETE;
    end
    else
    begin
        game_over_nxt = 0;
        game_state_nxt = WAIT;
    end
    
    score1_nxt = score1;
    score2_nxt = score2;    
end
endtask

task Player2_Missed;
begin
    game_state_nxt = WAIT;
    game_over_nxt = 0;
    score1_nxt = score1;
    score2_nxt = score2;    
end
endtask

task Player1_Wrong;
begin
    if(game_mode == NORMAL)
    begin
        game_over_nxt = 1;
        game_state_nxt = DELETE;
    end
    else
    begin
        game_over_nxt = 0;
        game_state_nxt = WAIT;
    end
    score1_nxt = score1;
    score2_nxt = score2;    
end
endtask

task Player2_Wrong;
begin
    game_state_nxt = WAIT;
    game_over_nxt = 0;        
    score1_nxt = score1;
    score2_nxt = score2;    
end
endtask

endmodule
