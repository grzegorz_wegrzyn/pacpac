//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   char_rom_16x16
 Author:        Krzysztof Sitko
 Version:       1.0
 Last modified: 2017-05-10
 Coding style:  safe, with FPGA sync reset
 Description:   chars to be displayed
 */
//////////////////////////////////////////////////////////////////////////////

module char_rom_16x16(
    input wire [7:0] char_xy,
    input wire [1:0] game_mod,
    input wire [6:0] score1,
    input wire [6:0] score2,
    output reg [6:0] char_code
    );
        
    //16 characters allowed in each localparam
    reg [0:127] line0   = "                ";        
    reg [0:127] line1   = "                "; //player1
    reg [0:127] line2   = "                ";
    reg [0:127] line3   = "                ";
    reg [0:127] line4   = "                "; //player2
    reg [0:127] line5   = "                "; 
    reg [0:127] line6   = "                ";
    reg [0:127] line7   = "                ";
    reg [0:127] line8   = "                ";
    reg [0:127] line9   = "                ";
    reg [0:127] line10  = "                ";
    reg [0:127] line11  = "                ";
    reg [0:127] line12  = "                ";
    reg [0:127] line13  = "                ";
    reg [0:127] line14  = " right click to ";
    reg [0:127] line15  = "   enter menu   ";
    
    reg [7:0] left_digit_1, right_digit_1;
    reg [7:0] left_digit_2, right_digit_2;
    
    always @*
        if(score1>=0 && score1<=9)
            begin
                left_digit_1  = 8'h30;     //czyli 0 w font'cie
                right_digit_1 = 8'h30 + score1;
            end        
        else if(score1>=10 && score1<=19)
            begin
                left_digit_1  = 8'h30 + 1;    //czyli +1 od zera, czyli kolejna po zerze, czyli jedynka
                right_digit_1 = 8'h30 + (score1 - 10);
            end
        else if(score1>=20 && score1<=29)
            begin
                left_digit_1  = 8'h30 + 2;    
                right_digit_1 = 8'h30 + (score1 - 20);
            end
        else if(score1>=30 && score1<=39)
            begin
                left_digit_1  = 8'h30 + 3;    
                right_digit_1 = 8'h30 + (score1 - 30);
            end
        else if(score1>=40 && score1<=49)
            begin
                left_digit_1  = 8'h30 + 4;   
                right_digit_1 = 8'h30 + (score1 - 40);
            end
        else if(score1>=50 && score1<=59)
            begin
                left_digit_1  = 8'h30 + 5;    
                right_digit_1 = 8'h30 + (score1 - 50);
            end
        else if(score1>=60 && score1<=69)
            begin
                left_digit_1  = 8'h30 + 6;   
                right_digit_1 = 8'h30 + (score1 - 60);
            end
        else if(score1>=70 && score1<=79)
            begin
                left_digit_1  = 8'h30 + 7;   
                right_digit_1 = 8'h30 + (score1 - 70);
            end
        else if(score1>=80 && score1<=89)
            begin
                left_digit_1  = 8'h30 + 8;    
                right_digit_1 = 8'h30 + (score1 - 80);
            end                         
        else
            begin
                left_digit_1  = 8'h30 + 9;    
                right_digit_1 = 8'h30 + (score1 - 90);
            end
    
    always @*
        if(score2>=0 && score2<=9)
            begin
                left_digit_2  = 8'h30;     //czyli 0 w font'cie
                right_digit_2 = 8'h30 + score2;
            end        
        else if(score2>=10 && score2<=19)
            begin
                left_digit_2  = 8'h30 + 1;    //czyli +1 od zera, czyli kolejna po zerze, czyli jedynka
                right_digit_2 = 8'h30 + (score2 - 10);
            end
        else if(score2>=20 && score2<=29)
            begin
                left_digit_2  = 8'h30 + 2;    
                right_digit_2 = 8'h30 + (score2 - 20);
            end
        else if(score2>=30 && score2<=39)
            begin
                left_digit_2  = 8'h30 + 3;    
                right_digit_2 = 8'h30 + (score2 - 30);
            end
        else if(score2>=40 && score2<=49)
            begin
                left_digit_2  = 8'h30 + 4;   
                right_digit_2 = 8'h30 + (score2 - 40);
            end
        else if(score2>=50 && score2<=59)
            begin
                left_digit_2  = 8'h30 + 5;    
                right_digit_2 = 8'h30 + (score2 - 50);
            end
        else if(score2>=60 && score2<=69)
            begin
                left_digit_2  = 8'h30 + 6;   
                right_digit_2 = 8'h30 + (score2 - 60);
            end
        else if(score2>=70 && score2<=79)
            begin
                left_digit_2  = 8'h30 + 7;   
                right_digit_2 = 8'h30 + (score2 - 70);
            end
        else if(score2>=80 && score2<=89)
            begin
                left_digit_2  = 8'h30 + 8;    
                right_digit_2 = 8'h30 + (score2 - 80);
            end                         
        else
            begin
                left_digit_2  = 8'h30 + 9;    
                right_digit_2 = 8'h30 + (score2 - 90);
            end
    
    always @*
        case (game_mod)
            2'b00:  //NORMAL
                begin
                    line1[0:111]   = "Player 1:     ";
                    line1[112:119] = left_digit_1;
                    line1[120:127] = right_digit_1;
                    line4   = "                ";   //empty, no player2
                end
            2'b01:  //MULTI
                begin
                    line1[0:111]   = "Player 1:     ";
                    line1[112:119] = left_digit_1;
                    line1[120:127] = right_digit_1;
                    line4[0:111]   = "Player 2:     ";   
                    line4[112:119] = left_digit_2;
                    line4[120:127] = right_digit_2;
                end
            2'b10: //REFLEX
                begin
                    line1[0:79]   = "Player 1: ";
                    line1[80:87] = left_digit_1;
                    line1[88:95] = right_digit_1;
                    line1[96:127]   = "/min";
                    line4   = "                ";   //empty, no player2
                end
            default:
                begin
                    line1   = "                ";
                    line4   = "                ";
                end
        endcase
    
    always @*
    if (char_xy[3:0] == 0)      //zerowa linijka
        case (char_xy[7:4])
            0:  char_code = line0[0:7];
            1:  char_code = line0[8:15];
            2:  char_code = line0[16:23];
            3:  char_code = line0[24:31];
            4:  char_code = line0[32:39];
            5:  char_code = line0[40:47];
            6:  char_code = line0[48:55];
            7:  char_code = line0[56:63];
            8:  char_code = line0[64:71];
            9:  char_code = line0[72:79];
            10:  char_code = line0[80:87];
            11:  char_code = line0[88:95];
            12:  char_code = line0[96:103];
            13:  char_code = line0[104:111];
            14:  char_code = line0[112:119];
            15:  char_code = line0[120:127];   
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 1) //pierwsza linijka
        case (char_xy[7:4])
            0:  char_code = line1[0:7];
            1:  char_code = line1[8:15];
            2:  char_code = line1[16:23];
            3:  char_code = line1[24:31];
            4:  char_code = line1[32:39];
            5:  char_code = line1[40:47];
            6:  char_code = line1[48:55];
            7:  char_code = line1[56:63];
            8:  char_code = line1[64:71];
            9:  char_code = line1[72:79];
            10:  char_code = line1[80:87];
            11:  char_code = line1[88:95];
            12:  char_code = line1[96:103];
            13:  char_code = line1[104:111];
            14:  char_code = line1[112:119];
            15:  char_code = line1[120:127];
            default: char_code = 0;    
        endcase
            
    else if (char_xy[3:0] == 2) //druga linijka
        case (char_xy[7:4])
            0:  char_code = line2[0:7];
            1:  char_code = line2[8:15];
            2:  char_code = line2[16:23];
            3:  char_code = line2[24:31];
            4:  char_code = line2[32:39];
            5:  char_code = line2[40:47];
            6:  char_code = line2[48:55];
            7:  char_code = line2[56:63];
            8:  char_code = line2[64:71];
            9:  char_code = line2[72:79];
            10:  char_code = line2[80:87];
            11:  char_code = line2[88:95];
            12:  char_code = line2[96:103];
            13:  char_code = line2[104:111];
            14:  char_code = line2[112:119];
            15:  char_code = line2[120:127];
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 3) //trzecia linijka
        case (char_xy[7:4])
            0:  char_code = line3[0:7];
            1:  char_code = line3[8:15];
            2:  char_code = line3[16:23];
            3:  char_code = line3[24:31];
            4:  char_code = line3[32:39];
            5:  char_code = line3[40:47];
            6:  char_code = line3[48:55];
            7:  char_code = line3[56:63];
            8:  char_code = line3[64:71];
            9:  char_code = line3[72:79];
            10:  char_code = line3[80:87];
            11:  char_code = line3[88:95];
            12:  char_code = line3[96:103];
            13:  char_code = line3[104:111];
            14:  char_code = line3[112:119];
            15:  char_code = line3[120:127]; 
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 4) //czwarta linijka
        case (char_xy[7:4])
            0:  char_code = line4[0:7];
            1:  char_code = line4[8:15];
            2:  char_code = line4[16:23];
            3:  char_code = line4[24:31];
            4:  char_code = line4[32:39];
            5:  char_code = line4[40:47];
            6:  char_code = line4[48:55];
            7:  char_code = line4[56:63];
            8:  char_code = line4[64:71];
            9:  char_code = line4[72:79];
            10:  char_code = line4[80:87];
            11:  char_code = line4[88:95];
            12:  char_code = line4[96:103];
            13:  char_code = line4[104:111];
            14:  char_code = line4[112:119];
            15:  char_code = line4[120:127];  
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 5) //piata linijka
        case (char_xy[7:4])
            0:  char_code = line5[0:7];
            1:  char_code = line5[8:15];
            2:  char_code = line5[16:23];
            3:  char_code = line5[24:31];
            4:  char_code = line5[32:39];
            5:  char_code = line5[40:47];
            6:  char_code = line5[48:55];
            7:  char_code = line5[56:63];
            8:  char_code = line5[64:71];
            9:  char_code = line5[72:79];
            10:  char_code = line5[80:87];
            11:  char_code = line5[88:95];
            12:  char_code = line5[96:103];
            13:  char_code = line5[104:111];
            14:  char_code = line5[112:119];
            15:  char_code = line5[120:127]; 
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 6) //szosta linijka
        case (char_xy[7:4])
            0:  char_code = line6[0:7];
            1:  char_code = line6[8:15];
            2:  char_code = line6[16:23];
            3:  char_code = line6[24:31];
            4:  char_code = line6[32:39];
            5:  char_code = line6[40:47];
            6:  char_code = line6[48:55];
            7:  char_code = line6[56:63];
            8:  char_code = line6[64:71];
            9:  char_code = line6[72:79];
            10:  char_code = line6[80:87];
            11:  char_code = line6[88:95];
            12:  char_code = line6[96:103];
            13:  char_code = line6[104:111];
            14:  char_code = line6[112:119];
            15:  char_code = line6[120:127];   
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 7) //siodma linijka
        case (char_xy[7:4])
            0:  char_code = line7[0:7];
            1:  char_code = line7[8:15];
            2:  char_code = line7[16:23];
            3:  char_code = line7[24:31];
            4:  char_code = line7[32:39];
            5:  char_code = line7[40:47];
            6:  char_code = line7[48:55];
            7:  char_code = line7[56:63];
            8:  char_code = line7[64:71];
            9:  char_code = line7[72:79];
            10:  char_code = line7[80:87];
            11:  char_code = line7[88:95];
            12:  char_code = line7[96:103];
            13:  char_code = line7[104:111];
            14:  char_code = line7[112:119];
            15:  char_code = line7[120:127];
            default: char_code = 0;  
        endcase
    
    else if (char_xy[3:0] == 8) //osma linijka
        case (char_xy[7:4])
            0:  char_code = line8[0:7];
            1:  char_code = line8[8:15];
            2:  char_code = line8[16:23];
            3:  char_code = line8[24:31];
            4:  char_code = line8[32:39];
            5:  char_code = line8[40:47];
            6:  char_code = line8[48:55];
            7:  char_code = line8[56:63];
            8:  char_code = line8[64:71];
            9:  char_code = line8[72:79];
            10:  char_code = line8[80:87];
            11:  char_code = line8[88:95];
            12:  char_code = line8[96:103];
            13:  char_code = line8[104:111];
            14:  char_code = line8[112:119];
            15:  char_code = line8[120:127];    
            default: char_code = 0; 
        endcase
    
    else if (char_xy[3:0] == 9) //dziewiata linijka
        case (char_xy[7:4])
            0:  char_code = line9[0:7];
            1:  char_code = line9[8:15];
            2:  char_code = line9[16:23];
            3:  char_code = line9[24:31];
            4:  char_code = line9[32:39];
            5:  char_code = line9[40:47];
            6:  char_code = line9[48:55];
            7:  char_code = line9[56:63];
            8:  char_code = line9[64:71];
            9:  char_code = line9[72:79];
            10:  char_code = line9[80:87];
            11:  char_code = line9[88:95];
            12:  char_code = line9[96:103];
            13:  char_code = line9[104:111];
            14:  char_code = line9[112:119];
            15:  char_code = line9[120:127];
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 10) //dziesiata linijka
        case (char_xy[7:4])
            0:  char_code = line10[0:7];
            1:  char_code = line10[8:15];
            2:  char_code = line10[16:23];
            3:  char_code = line10[24:31];
            4:  char_code = line10[32:39];
            5:  char_code = line10[40:47];
            6:  char_code = line10[48:55];
            7:  char_code = line10[56:63];
            8:  char_code = line10[64:71];
            9:  char_code = line10[72:79];
            10:  char_code = line10[80:87];
            11:  char_code = line10[88:95];
            12:  char_code = line10[96:103];
            13:  char_code = line10[104:111];
            14:  char_code = line10[112:119];
            15:  char_code = line0[120:127];  
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 11) //jedenasta linijka
        case (char_xy[7:4])
            0:  char_code = line11[0:7];
            1:  char_code = line11[8:15];
            2:  char_code = line11[16:23];
            3:  char_code = line11[24:31];
            4:  char_code = line11[32:39];
            5:  char_code = line11[40:47];
            6:  char_code = line11[48:55];
            7:  char_code = line11[56:63];
            8:  char_code = line11[64:71];
            9:  char_code = line11[72:79];
            10:  char_code = line11[80:87];
            11:  char_code = line11[88:95];
            12:  char_code = line11[96:103];
            13:  char_code = line11[104:111];
            14:  char_code = line11[112:119];
            15:  char_code = line0[120:127]; 
            default: char_code = 0; 
        endcase
    
    else if (char_xy[3:0] == 12) //dwunasta linijka
        case (char_xy[7:4])
            0:  char_code = line12[0:7];
            1:  char_code = line12[8:15];
            2:  char_code = line12[16:23];
            3:  char_code = line12[24:31];
            4:  char_code = line12[32:39];
            5:  char_code = line12[40:47];
            6:  char_code = line12[48:55];
            7:  char_code = line12[56:63];
            8:  char_code = line12[64:71];
            9:  char_code = line12[72:79];
            10:  char_code = line12[80:87];
            11:  char_code = line12[88:95];
            12:  char_code = line12[96:103];
            13:  char_code = line12[104:111];
            14:  char_code = line12[112:119];
            15:  char_code = line12[120:127];  
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 13) //trzynasta linijka
        case (char_xy[7:4])
            0:  char_code = line13[0:7];
            1:  char_code = line13[8:15];
            2:  char_code = line13[16:23];
            3:  char_code = line13[24:31];
            4:  char_code = line13[32:39];
            5:  char_code = line13[40:47];
            6:  char_code = line13[48:55];
            7:  char_code = line13[56:63];
            8:  char_code = line13[64:71];
            9:  char_code = line13[72:79];
            10:  char_code = line13[80:87];
            11:  char_code = line13[88:95];
            12:  char_code = line13[96:103];
            13:  char_code = line13[104:111];
            14:  char_code = line13[112:119];
            15:  char_code = line13[120:127]; 
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 14) //czternasta linijka
        case (char_xy[7:4])
            0:  char_code = line14[0:7];
            1:  char_code = line14[8:15];
            2:  char_code = line14[16:23];
            3:  char_code = line14[24:31];
            4:  char_code = line14[32:39];
            5:  char_code = line14[40:47];
            6:  char_code = line14[48:55];
            7:  char_code = line14[56:63];
            8:  char_code = line14[64:71];
            9:  char_code = line14[72:79];
            10:  char_code = line14[80:87];
            11:  char_code = line14[88:95];
            12:  char_code = line14[96:103];
            13:  char_code = line14[104:111];
            14:  char_code = line14[112:119];
            15:  char_code = line14[120:127]; 
            default: char_code = 0;
        endcase
    
    else if (char_xy[3:0] == 15) //pietnasta linijka
        case (char_xy[7:4])
            0:  char_code = line15[0:7];
            1:  char_code = line15[8:15];
            2:  char_code = line15[16:23];
            3:  char_code = line15[24:31];
            4:  char_code = line15[32:39];
            5:  char_code = line15[40:47];
            6:  char_code = line15[48:55];
            7:  char_code = line15[56:63];
            8:  char_code = line15[64:71];
            9:  char_code = line15[72:79];
            10:  char_code = line15[80:87];
            11:  char_code = line15[88:95];
            12:  char_code = line15[96:103];
            13:  char_code = line15[104:111];
            14:  char_code = line15[112:119];
            15:  char_code = line15[120:127]; 
            default: char_code = 0; 
        endcase
        
    else char_code = 0;
endmodule
