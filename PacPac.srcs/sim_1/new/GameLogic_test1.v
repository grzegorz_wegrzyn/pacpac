//////////////////////////////////////////////////////////////////////////////
/*
 Module name:   Gamelogic test unit 1
 Author:        Grzegorz Wegrzyn
 Version:       1.0
 Last modified: 2017-05-31
 Coding style: safe, with FPGA sync reset
 Description:  GameLogic test unit 1.
 */
//////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps

module GameLogic_test1();

//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg clk, rst, left, right = 0;
    reg [11:0] xpos, ypos = 0;
    wire [2:0] score;
    wire point;
    Logic my_game(
        .clk(clk),
        .rst(rst),
        .mouse_xpos(xpos),
        .mouse_ypos(ypos),
        .mouse_left(left),
        .mouse_right(right),
        .xy_t1(0),
        .score1(score),
        .point1(point)
    );
    
    Counter #(.WIDTH(3)) cnt (
        .clk(clk),
        .rst(rst),
        .point(point),
        .counter(score)
    );
//------------------------------------------------------------------------------
// output register with sync reset
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// logic
//------------------------------------------------------------------------------

always 
begin
    #5
    clk = 1;
    #5
    clk = 0;
end

initial
begin
    repeat(10)@(posedge clk);
    xpos = 280;
    ypos = 280;
    left = 1;
    @(posedge clk);
    left = 0;
    @(posedge clk);
    xpos = 5;
    ypos = 5;
    
    repeat(8)
    begin
        repeat(10)@(posedge clk);
        left = 1;
        @(posedge clk);
        left = 0;
    end
end
    
endmodule
